# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.3
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# cd ..

import os

os.chdir("data/Vues fusionnées/")

import pandas as pd

df = pd.read_csv("2_actions.csv", sep=';')

df.ca_sup = df.ca_sup.replace(pd.np.inf, pd.np.nan)

df.to_csv("actions.csv", sep=";", index=False)


