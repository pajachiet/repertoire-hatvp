# Exploration du répertoire des représentants d'intérêts 

## Installation

Prérequis : Docker, Docker-Compose, python3, pip3, unzip

Sur un server Ubuntu 18
```
apt  install docker
apt  install docker-compose
apt install python3-pip
apt install unzip
git clone https://gitlab.com/pajachiet/repertoire-hatvp.git
```

- En production
    - Créer le fichier d'environnement `.env` depuis le template (`cp env-template .env`). 
    - Ajouter des mots de passes dans ce fichier pour protéger la connexion aux bases.
    
- Démarrage des services : `docker-compose up -d`
- Télécharger les données : `./download_data.sh`
- Préparer les données
    - installer environnement virtuel python `make install`
    - activer environnement `source venv/bin/activate`
    - préparer les données `python notebooks/prepare_data.py`
- Peupler les tables postgres : `docker-compose exec postgres bash /home/data/load_table.sh`
- Se connecter à Metabase [http://localhost:3000](http://localhost:3000)
    - créer un compte administrateur
    - connecter la base PostgreSQL contenant les données (paramètre `postgres` partout)
        - Nom : `postgres` (ou peu importe)
        - Hôte : `postgres` (nom de l'hôte via Docker)
        - Port : `5432`
        - Nom de la base de données : `postgres`
        - Nom de l'utilisateur : `postgres`
        - Mot de passe : vide (ou celui indiqué dans `.env)
    - aller dans le tableau d'administration
        - Si la base n'a pas été ajouté (bug ?), recommencer
        - Dans `General`, modifier le paramètre `Adaptation des noms de table et de champ` par `Remplacer seulement les soulignés et tirés par un espace`
    - Metabase est prêt pour l'exploration, en tant qu'utilisateur ou administrateur 

## Autres manipulations
 
Se connecter à l'invite de commande postgres : `docker-compose exec postgres psql -U postgres`

Arrêter les conteneurs : `docker-compose stop`

Pour aller plus loin, se référer à la documentation de [Docker-Compose](https://docs.docker.com/compose/reference/overview/).
