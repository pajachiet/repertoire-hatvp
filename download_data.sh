#!/usr/bin/env bash

cd data
wget https://www.hatvp.fr/agora/opendata/csv/Vues_Fusionnees.zip
wget https://www.hatvp.fr/agora/opendata/csv/Vues_Intermediaires.zip
wget https://www.hatvp.fr/agora/opendata/csv/Vues_Separees_CSV.zip

unzip Vues_Fusionnees.zip
unzip Vues_Intermediaires.zip
unzip Vues_Separees_CSV.zip