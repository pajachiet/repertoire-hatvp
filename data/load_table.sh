#!/bin/bash
set -e

cd /home/data

for sql in `find . -name "*sql"`
do
echo ${sql}

command=`cat ${sql}`
psql -v ON_ERROR_STOP=1 --username postgres <<-EOSQL
    ${command}
EOSQL

done
